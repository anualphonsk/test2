// eslint-disable-next-line no-unused-vars
import logo from './logo.svg';
import './assets/css/react_app.css';
import Login from './client/components/Login';
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import HomePage from './client/components/HomePage';

function App() {
  return (
    <Router>
    <div>
      <Switch>
        <Route path="/home">
          <HomePage/>
        </Route>     
        <Route path="/">
          <Login />
        </Route>
      </Switch>
    </div>
  </Router>
);
}
 

export default App;
