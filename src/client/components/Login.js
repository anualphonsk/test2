import React from "react";
import { useForm, } from "react-hook-form";
import { useRouteMatch, useHistory, useParams, useLocation } from "react-router-dom";


function Login() {


    const { register, handleSubmit, errors, setError } = useForm();

    // const { push } = useHistory();

    const onSubmit = (data) => {

        if (data.name === 'admin' && data.password === 'password') {
            console.log("kk");
            window.location.href = '/home'
        }
    }

    return (
        <React.Fragment>
            <div class="container mt-5 mb-5 d-flex justify-content-center">
                <div class="card px-1 py-4">
                    <div class="card-body"></div>
                    <form onSubmit={handleSubmit(onSubmit)}>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="name" id="name" {...register('name', { required: true, maxLength: 30 })} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="password" id="password" {...register('password', { required: true, maxLength: 30 })} />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary btn-block confirm-button" type="submit" value="Submit" >Login</button>
                    </form>
                </div>
            </div>

        </React.Fragment>


    );
}
export default Login;