import React, { useEffect, useState } from "react";

function HomePage() {

    const [tableState, setTableState] = useState([])



    useEffect(() => {
        fetch("https://randomuser.me/api/0.8/?results=20")
            .then(res => res.json())

            .then(data => setTableState(data.results)
            )
    }, [])

    console.log("DATAAAA", tableState);
    return (
        <React.Fragment>
            <div className="App">
                <ul>
                    {tableState.map((e) => {
                        return (
                            <div>
                                <li>Name:
                                {e.user.name.first}
                                </li>
                                <li>
                                    Gender:{e.user.gender}
                                </li>
                                <li>
                                    Email:{e.user.email}
                                </li>
                                <li>
                                    Password:{e.user.password}
                                </li>
                                <li>
                                    UserName:{e.user.username}
                                </li>
                                <li>
                                    Phone Number:{e.user.cell}
                                </li>
                                <li>
                                    DOB:{e.user.dob}
                                </li>
                            </div>
                        )
                    })}

                </ul>
            </div>
        </React.Fragment>
    )
}
export default HomePage;